package net.minecraft.src;
import net.minecraft.src.forge.*;
                
public class mod_Drinks extends BaseMod{  
	
	public static final Block WoodPane = new BlockWoodPane(179, 8).setBlockName("woodpane");
	public static final Item Bottle = new ItemBottle(161).setIconIndex(0).setItemName("bottle");
	public static final Item BottleWater = new ItemBottleWater(162).setIconIndex(1).setItemName("bottlewater");
	public static final Item BottleRum = new ItemBottleRum(163, 4, false).setIconIndex(2).setItemName("bootlerum");
	public static final Item BottleTequila = new ItemBottleTequila(164, 4, false).setIconIndex(3).setItemName("bottletequila");
	public static final Item BottleSidr = new ItemBottleSidr(165, 8, false).setIconIndex(4).setItemName("bottlesidr");
	public static final Block RumCrate = new BlockRumCrate(166, 6).setHardness(3F).setResistance(4F).setLightValue(0.0F).setBlockName("rumcrate");
	public static final Block TequilaCrate = new BlockTequilaCrate(167, 22).setHardness(3F).setResistance(4F).setLightValue(0.0F).setBlockName("tequilacrate");
	public static final Block SidrCrate = new BlockSidrCrate(168, 38).setHardness(3F).setResistance(4F).setLightValue(0.0F).setBlockName("sidrcrate");
	public static final Block BlockGrape = new BlockGrape(169, 9).setBlockName("blockgrape");
	public static final Item Grape = new ItemGrape(169, 3, false).setIconIndex(24).setItemName("grape");
	public static final Item GrapeSeeds = new ItemGrapeSeeds(170, BlockGrape.blockID, WoodPane.blockID).setIconIndex(25).setItemName("grapeseeds");
	public static final Block BlockWhiteGrape = new BlockWhiteGrape(171, 9).setBlockName("blockwhitegrape");
	public static final Item WhiteGrape = new ItemWhiteGrape(172, 3, false).setIconIndex(40).setItemName("whitegrape");
	public static final Item WhiteGrapeSeeds = new ItemWhiteGrapeSeeds(173, BlockWhiteGrape.blockID, WoodPane.blockID).setIconIndex(41).setItemName("whitegrapeseeds");
	public static final Item GrapeCutter = new ItemGrapeCutter(174, EnumToolMaterial.IRON).setIconIndex(23).setItemName("grapecutter");
	public static final Item BottleVine = new ItemBottleVine(175, 4, false).setIconIndex(16).setItemName("bottlevine");
	public static final Item BottleWhiteVine = new ItemBottleWhiteVine(176, 4, false).setIconIndex(17).setItemName("bottlewhitevine");
	public static final Block VineCrate = new BlockVineCrate(177, 54).setHardness(3F).setResistance(4F).setLightValue(0.0F).setBlockName("vinecrate");
	public static final Block WhiteVineCrate = new BlockWhiteVineCrate(178, 70).setHardness(3F).setResistance(4F).setLightValue(0.0F).setBlockName("whitevinecrate");
	
	public static final Block BlockRise = new BlockRise(180, 14).setBlockName("blockrise");
	public static final Item Rise = new ItemRise(181, BlockRise.blockID, Block.tilledField.blockID).setIconIndex(15).setItemName("rise");
	public static final Item RiseKnife = new ItemGrapeCutter(182, EnumToolMaterial.IRON).setIconIndex(39).setItemName("riseknife");
	public static final Item BottleShnaps = new ItemBottleShnaps(183, 4, false).setIconIndex(18).setItemName("bottleshnaps");
	public static final Block ShnapsCrate = new BlockShnapsCrate(184, 86).setHardness(3F).setResistance(4F).setLightValue(0.0F).setBlockName("shnapscrate");
	public static final Item RiseTonic = new ItemRiseTonic(185).setIconIndex(31).setItemName("risetonic");
	
	//TODO ������� �������� �� ���� � �������� �� � ����� ������
	//��� - �� - 30 ���. �������� 1 ������. (������ ������ ������)
	//������ - ��. 10 ���. �������� 3 ������ 
	//�������� ���� - ����������� 5 ������, ������� 3 ���
	//������� ���� - ���� �� 1 ������, �� 10 ������, ���������� 10
	//������� ���� - �������������� ����� 20 ������, ���������� 25 ���
	
	
	
	public String getVersion(){               
        return "0.0.4";       
    }        
    public void load(){               
        MinecraftForgeClient.preloadTexture("/drinks/items.png");
		
		ModLoader.addName(Bottle, "�������");
		ModLoader.addRecipe(new ItemStack(Bottle, 4), new Object [] {
		"#", "#", "#", 
		Character.valueOf('#'), Block.glass
		});
		
		ModLoader.addName(BottleWater, "������� ����");
		
		ModLoader.addName(BottleRum, "������� ����");
		ModLoader.addRecipe(new ItemStack(BottleRum, 1), new Object[] {
		"a", "b", "c",
		Character.valueOf('a'), new ItemStack(Item.dyePowder, 1, 1),
		Character.valueOf('b'), new ItemStack(Item.dyePowder, 1, 3),
		Character.valueOf('c'), BottleWater
		});
		
		ModLoader.addName(BottleTequila, "������� ������");
		ModLoader.addRecipe(new ItemStack(BottleTequila, 1), new Object[] {
		"a", "b", "c",
		Character.valueOf('a'), new ItemStack(Item.dyePowder, 1, 2),
		Character.valueOf('b'), Block.cactus,
		Character.valueOf('c'), BottleWater
		});
		
		ModLoader.addName(BottleSidr, "������� �����");
		ModLoader.addRecipe(new ItemStack(BottleRum, 1), new Object[] {
		"a", "b", "c",
		Character.valueOf('a'), Item.appleRed,
		Character.valueOf('b'), Item.appleGold,
		Character.valueOf('c'), BottleWater
		});
		
		ModLoader.registerBlock(RumCrate);
		ModLoader.addName(RumCrate, "���� ����");
		ModLoader.addRecipe(new ItemStack(RumCrate, 1), new Object[] {
		"aaa", "aba", "aaa",
		Character.valueOf('a'), BottleRum,
		Character.valueOf('b'), Block.chest
		});
		ModLoader.addRecipe(new ItemStack(BottleRum, 8), new Object[] {
		"a",
		Character.valueOf('a'), RumCrate
		});
		
		ModLoader.registerBlock(TequilaCrate);
		ModLoader.addName(TequilaCrate, "���� ������");
		ModLoader.addRecipe(new ItemStack(TequilaCrate, 1), new Object[] {
		"aaa", "aba", "aaa",
		Character.valueOf('a'), BottleTequila,
		Character.valueOf('b'), Block.chest
		});
		ModLoader.addRecipe(new ItemStack(BottleTequila, 8), new Object[] {
		"a",
		Character.valueOf('a'), TequilaCrate
		});
		
		ModLoader.registerBlock(SidrCrate);
		ModLoader.addName(SidrCrate, "���� �����");
		ModLoader.addRecipe(new ItemStack(SidrCrate, 1), new Object[] {
		"aaa", "aba", "aaa",
		Character.valueOf('a'), BottleSidr,
		Character.valueOf('b'), Block.chest
		});
		ModLoader.addRecipe(new ItemStack(BottleSidr, 8), new Object[] {
		"a",
		Character.valueOf('a'), SidrCrate
		});
		
		ModLoader.registerBlock(BlockGrape);
		ModLoader.addName(BlockGrape, "�������� ��������");
		
		ModLoader.addName(Grape, "��������");
		ModLoader.addName(GrapeSeeds, "���� ���������");
		ModLoader.addRecipe(new ItemStack(GrapeSeeds, 4), new Object[] {
		"a",
		Character.valueOf('a'), Grape
		});
		
		ModLoader.registerBlock(BlockWhiteGrape);
		ModLoader.addName(BlockWhiteGrape, "�������� ����� ��������");
		
		ModLoader.addName(WhiteGrape, "����� ��������");
		ModLoader.addName(WhiteGrapeSeeds, "���� ������ ���������");
		ModLoader.addRecipe(new ItemStack(WhiteGrapeSeeds, 4), new Object[] {
		"a",
		Character.valueOf('a'), WhiteGrape
		});
		
		ModLoader.addName(GrapeCutter, "�������");
		ModLoader.addRecipe(new ItemStack(GrapeCutter, 1), new Object[] {
		"dad", "acb", "dbd",
		Character.valueOf('a'), Item.ingotIron,
		Character.valueOf('b'), Item.stick,
		Character.valueOf('c'), Block.button
		});
		
		ModLoader.addName(BottleVine, "������� ����");
		ModLoader.addRecipe(new ItemStack(BottleVine, 1), new Object[] {
		"dad", "dbd", "dcd",
		Character.valueOf('a'), new ItemStack(Item.dyePowder, 1, 1),
		Character.valueOf('b'), Grape,
		Character.valueOf('c'), BottleWater,
		Character.valueOf('d'), Item.sugar
		});
		
		ModLoader.addName(BottleWhiteVine, "������� ������ ����");
		ModLoader.addRecipe(new ItemStack(BottleWhiteVine, 1), new Object[] {
		"dad", "dbd", "dcd",
		Character.valueOf('a'), new ItemStack(Item.dyePowder, 1, 1),
		Character.valueOf('b'), WhiteGrape,
		Character.valueOf('c'), BottleWater,
		Character.valueOf('d'), Item.sugar
		});
		
		ModLoader.registerBlock(VineCrate);
		ModLoader.addName(VineCrate, "���� ����");
		ModLoader.addRecipe(new ItemStack(VineCrate, 1), new Object[] {
		"aaa", "aba", "aaa",
		Character.valueOf('a'), BottleVine,
		Character.valueOf('b'), Block.chest
		});
		ModLoader.addRecipe(new ItemStack(BottleVine, 8), new Object[] {
		"a",
		Character.valueOf('a'), VineCrate
		});
		
		ModLoader.registerBlock(WhiteVineCrate);
		ModLoader.addName(WhiteVineCrate, "���� ������ ����");
		ModLoader.addRecipe(new ItemStack(WhiteVineCrate, 1), new Object[] {
		"aaa", "aba", "aaa",
		Character.valueOf('a'), BottleWhiteVine,
		Character.valueOf('b'), Block.chest
		});
		ModLoader.addRecipe(new ItemStack(BottleWhiteVine, 8), new Object[] {
		"a",
		Character.valueOf('a'), WhiteVineCrate
		});
		
		ModLoader.registerBlock(WoodPane);
		ModLoader.addName(WoodPane, "���������� ������");
		ModLoader.addRecipe(new ItemStack(WoodPane, 1), new Object[] {
		"aaa", "aaa", "aaa",
		Character.valueOf('a'), Item.stick
		});
		
		ModLoader.registerBlock(BlockRise);
		ModLoader.addName(WoodPane, "�������� ���");
		ModLoader.addName(Rise, "���");
		
		ModLoader.addName(RiseKnife, "��� ��� ����� ����");
		ModLoader.addRecipe(new ItemStack(RiseKnife, 1), new Object[] {
		"a", "b",
		Character.valueOf('a'), Item.ingotIron,
		Character.valueOf('b'), Item.stick
		});
		
		ModLoader.addName(BottleShnaps, "������� �������� ������");
		ModLoader.addRecipe(new ItemStack(BottleShnaps, 1), new Object[] {
		"aaa", "aba", "aca",
		Character.valueOf('a'), RiseTonic,
		Character.valueOf('b'), Item.sugar,
		Character.valueOf('c'), BottleWater,
		});
		
		ModLoader.registerBlock(ShnapsCrate);
		ModLoader.addName(ShnapsCrate, "���� �������� ������");
		ModLoader.addRecipe(new ItemStack(ShnapsCrate, 1), new Object[] {
		"aaa", "aba", "aaa",
		Character.valueOf('a'), BottleShnaps,
		Character.valueOf('b'), Block.chest
		});
		
		ModLoader.addRecipe(new ItemStack(BottleShnaps, 8), new Object[] {
		"a",
		Character.valueOf('a'), ShnapsCrate
		});
		
		ModLoader.addName(RiseTonic, "����������� ���");
		ModLoader.addRecipe(new ItemStack(RiseTonic, 1), new Object[] {
		"aaa", "aaa", "aaa",
		Character.valueOf('a'), Rise
		});
    } 
}