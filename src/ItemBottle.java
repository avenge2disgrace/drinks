package net.minecraft.src;
import net.minecraft.src.forge.ITextureProvider;

public class ItemBottle extends Item implements ITextureProvider{

    public ItemBottle(int i){
        super(i);
        maxStackSize = 8;              
    }
	public String getTextureFile(){
        return "/drinks/items.png";
    }
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
		MovingObjectPosition var4 = this.getMovingObjectPositionFromPlayer(par2World, par3EntityPlayer, true);
		
        if (var4 == null)
        {
            return par1ItemStack;
        }
        else
        {
            if (var4.typeOfHit == EnumMovingObjectType.TILE)
            {
                int var5 = var4.blockX;
                int var6 = var4.blockY;
                int var7 = var4.blockZ;

                if (!par2World.canMineBlock(par3EntityPlayer, var5, var6, var7))
                {
                    return par1ItemStack;
                }

                if (!par3EntityPlayer.canPlayerEdit(var5, var6, var7))
                {
                    return par1ItemStack;
                }

                if (par2World.getBlockMaterial(var5, var6, var7) == Material.water)
                {
                    --par1ItemStack.stackSize;

                    if (par1ItemStack.stackSize <= 0)
                    {
                        return new ItemStack(mod_Drinks.BottleWater);
                    }

                    if (!par3EntityPlayer.inventory.addItemStackToInventory(new ItemStack(mod_Drinks.BottleWater)))
                    {
                        par3EntityPlayer.dropPlayerItem(new ItemStack(mod_Drinks.BottleWater, 1, 0));
                    }
                }
            }

            return par1ItemStack;
        }
    }
}
