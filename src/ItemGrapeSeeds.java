package net.minecraft.src;
import net.minecraft.src.forge.ITextureProvider;

public class ItemGrapeSeeds extends Item implements ITextureProvider{
	private int blockType;
	private int soilBlockID;
	public ItemGrapeSeeds(int i, int j, int k){
		super(i);
		blockType = j;
		soilBlockID = k;
	}

	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7){
		if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6) || !par2EntityPlayer.canPlayerEdit(par4, par5, par6)){
			return false;
		}
		int i = par3World.getBlockId(par4, par5, par6);
		if (i == soilBlockID){
			par3World.setBlockWithNotify(par4, par5, par6, blockType);
			par1ItemStack.stackSize--;
			return true;
		}
		else{
			return false;
		}
		
	}
	public String getTextureFile(){
        return "/drinks/items.png";
    }
}