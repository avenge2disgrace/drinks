package net.minecraft.src;
public class ItemRiseKnife extends Item{
        public ItemRiseKnife(int par1, EnumToolMaterial par2EnumToolMaterial){
                super(par1);
                maxStackSize = 1;
                setMaxDamage(par2EnumToolMaterial.getMaxUses());
        }
		public String getTextureFile(){
			return "/drinks/items.png";
		}
        public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7){
                if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6)){
                        return false;
                }
                int i = par3World.getBlockId(par4, par5, par6);
                int j = par3World.getBlockId(par4, par5 + 1, par6);
                if (par7 != 0 && j == 0 && i == mod_Drinks.BlockRise.blockID){
                        if (par3World.isRemote){
                                return true;
                        }
                        else{
                                //par3World.setBlockWithNotify(par4, par5, par6, block.blockID);
                                par1ItemStack.damageItem(1, par2EntityPlayer);
                                return true;
                        }
                }
                else{
                        return false;
                }
        }

        public boolean isFull3D(){
                return true;
        }
}