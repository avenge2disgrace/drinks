package net.minecraft.src;
import net.minecraft.src.forge.ITextureProvider;

public class ItemRiseTonic extends Item implements ITextureProvider{

    public ItemRiseTonic(int i){
        super(i);
        maxStackSize = 32;              
    }
	public String getTextureFile(){
        return "/drinks/items.png";
    }
}
