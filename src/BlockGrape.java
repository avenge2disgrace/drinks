package net.minecraft.src;
import java.util.Random;
import net.minecraft.src.forge.ITextureProvider;

public class BlockGrape extends BlockFlower implements ITextureProvider{
	public BlockGrape(int i, int j){
		super(i, j, Material.plants);
		blockIndexInTexture = j;
		setTickRandomly(true);
		float var3 = 0.125F;
        //this.setBlockBounds(1.0F, 1.0F, 1.0F - var3, 1.0F, 1.0F, 1.0F);
	}
	protected boolean canThisPlantGrowOnThisBlockID(int par1){
		return par1 == mod_Drinks.WoodPane.blockID;
	}
	public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random){
		super.updateTick(par1World, par2, par3, par4, par5Random);
		if (par1World.getBlockLightValue(par2, par3 + 1, par4) >= 9){
			int i = par1World.getBlockMetadata(par2, par3, par4);
			if (i < 7){
				float f = getGrowthRate(par1World, par2, par3, par4);
				if (par5Random.nextInt(4) == 0){
					i++;
					par1World.setBlockMetadataWithNotify(par2, par3, par4, i);
				}
			}
		}
	}
	public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer){
		ItemStack itemstack = entityplayer.inventory.getCurrentItem();
		if(itemstack != null && itemstack.itemID == Item.dyePowder.shiftedIndex){
			if(itemstack.getItemDamage() == 15){
				world.setBlockMetadataWithNotify(i, j, k, 7);
				itemstack.stackSize--;
				world.notifyBlockChange(i, j, k, 0);
			}
		}
		if(itemstack != null && itemstack.itemID == mod_Drinks.GrapeCutter.shiftedIndex){
			int ii = world.getBlockMetadata(i, j, k);
			if (ii == 7){
				float f = 0.7F;
				float f1 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
				float f2 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
				float f3 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
				
				EntityItem entityitem = new EntityItem(world, (float)i + f1, (float)j + f2, (float)k + f3, new ItemStack(mod_Drinks.Grape));
				entityitem.delayBeforeCanPickup = 10;
				world.spawnEntityInWorld(entityitem);
				world.setBlockMetadataWithNotify(i, j, k, 3);
				world.notifyBlockChange(i, j, k, 0);
			}
		}
		return super.blockActivated(world, i, j, k, entityplayer);
	}
	private float getGrowthRate(World par1World, int par2, int par3, int par4){
		float f = 1.0F;
		int i = par1World.getBlockId(par2, par3, par4 - 1);
		int j = par1World.getBlockId(par2, par3, par4 + 1);
		int k = par1World.getBlockId(par2 - 1, par3, par4);
		int l = par1World.getBlockId(par2 + 1, par3, par4);
		int i1 = par1World.getBlockId(par2 - 1, par3, par4 - 1);
		int j1 = par1World.getBlockId(par2 + 1, par3, par4 - 1);
		int k1 = par1World.getBlockId(par2 + 1, par3, par4 + 1);
		int l1 = par1World.getBlockId(par2 - 1, par3, par4 + 1);
		boolean flag = k == blockID || l == blockID;
		boolean flag1 = i == blockID || j == blockID;
		boolean flag2 = i1 == blockID || j1 == blockID || k1 == blockID || l1 == blockID;
		for (int i2 = par2 - 1; i2 <= par2 + 1; i2++){
			for (int j2 = par4 - 1; j2 <= par4 + 1; j2++){
				int k2 = par1World.getBlockId(i2, par3 - 1, j2);
				float f1 = 0.0F;
				if (k2 == mod_Drinks.WoodPane.blockID){
					f1 = 1.0F;
					if (par1World.getBlockMetadata(i2, par3 - 1, j2) > 0){
						f1 = 3F;
					}
				}
				if (i2 != par2 || j2 != par4){
					f1 = 3F;
				}
				f += f1;
			}
		}
		if (flag2 || flag && flag1){
			f /= 2.0F;
		}
		return f;
	}
	public int getRenderType(){
		return 1;
	}
	public void dropBlockAsItemWithChance(World par1World, int par2, int par3, int par4, int par5, float par6, int par7){
		super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5, par6, 0);
		if (par1World.isRemote){
			return;
		}
		int i = 3 + par7;
		for (int j = 0; j < i; j++){
			if (par1World.rand.nextInt(15) <= par5){
				float f = 0.7F;
				float f1 = par1World.rand.nextFloat() * f + (1.0F - f) * 0.5F;
				float f2 = par1World.rand.nextFloat() * f + (1.0F - f) * 0.5F;
				float f3 = par1World.rand.nextFloat() * f + (1.0F - f) * 0.5F;
				EntityItem entityitem = new EntityItem(par1World, (float)par2 + f1, (float)par3 + f2, (float)par4 + f3, new ItemStack(mod_Drinks.GrapeSeeds));
				entityitem.delayBeforeCanPickup = 10;
				par1World.spawnEntityInWorld(entityitem);
			}
		}
	}
	public int idDropped(int i, Random random, int j){
		if (i == 7){
			return mod_Drinks.Grape.shiftedIndex;
		}
		else{
			return -1;
		}
	}
	public int quantityDropped(Random random){
		return 1;
	}
	public int getBlockTextureFromSideAndMetadata(int i, int j){
		if(j == 0){
			return blockIndexInTexture;
		}
		if(j == 1){
			return blockIndexInTexture + 1;
		}
		if(j == 2){
			return blockIndexInTexture + 2;
		}
		if(j == 3){
			return blockIndexInTexture + 2;
		}
		if(j == 4){
			return blockIndexInTexture + 2;
		}
		if(j == 5){
			return blockIndexInTexture + 2;
		}
		if(j == 6){
			return blockIndexInTexture + 2;
		}
		if(j == 7){
			return blockIndexInTexture + 3;
		}
		return j;
	}
	public String getTextureFile(){
        return "/drinks/items.png";
    }
	public boolean isOpaqueCube()
    {
        return false;
    }
	public boolean canPlaceBlockAt(World par1World, int par2, int par3, int par4)
    {
        int var5 = par1World.getBlockId(par2, par3 - 1, par4);
		if (var5 != this.blockID && var5 != mod_Drinks.BlockWhiteGrape.blockID && var5 != Block.dirt.blockID && var5 != Block.grass.blockID && var5 != mod_Drinks.WoodPane.blockID){
			return false;
		}
		else{
			return true;
		}
    }
	public boolean canBlockStay(World par1World, int par2, int par3, int par4)
    {
        return this.canPlaceBlockAt(par1World, par2, par3, par4);
    }
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        float var5 = 0.125F;
        return AxisAlignedBB.getBoundingBoxFromPool((double)((float)par2 + var5), (double)par3, (double)((float)par4 + var5), (double)((float)(par2 + 1) - var5), (double)((float)(par3 + 1) - var5), (double)((float)(par4 + 1) - var5));
    }

    /**
     * Returns the bounding box of the wired rectangular prism to render.
     */
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        float var5 = 0.125F;
        return AxisAlignedBB.getBoundingBoxFromPool((double)((float)par2 + var5), (double)par3, (double)((float)par4 + var5), (double)((float)(par2 + 1) - var5), (double)(par3 + 1), (double)((float)(par4 + 1) - var5));
    }
}