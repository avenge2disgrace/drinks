package net.minecraft.src;
import java.util.Random;
import net.minecraft.src.forge.ITextureProvider;

public class BlockTequilaCrate extends Block implements ITextureProvider{
    public BlockTequilaCrate(int i, int j){
        super(i, j, Material.wood);
    }
    public int idDropped(int i, Random random, int j){
        return mod_Drinks.TequilaCrate.blockID;
    }
    public int quantityDropped(Random random){
        return 1;
    }
	public String getTextureFile(){
        return "/drinks/items.png";
    }
	public int getBlockTextureFromSideAndMetadata(int i, int j){
        return getBlockTextureFromSide(i);
    }
	public int getBlockTextureFromSide(int i){
        if (i == 0){
            return 7;
        }
        if (i == 1){
            return 5;
        }
        else{
            return this.blockIndexInTexture;
        }
    }
}