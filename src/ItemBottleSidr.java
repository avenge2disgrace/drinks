package net.minecraft.src;
import net.minecraft.src.forge.ITextureProvider;

public class ItemBottleSidr extends ItemFood implements ITextureProvider{

    public ItemBottleSidr(int i, int j, boolean k){
        super(i, j, 0.4F, k);
        maxStackSize = 8;              
    }
	public String getTextureFile(){
        return "/drinks/items.png";
    }
	public boolean hasEffect(ItemStack par1ItemStack)
    {
        return true;
    }

    /**
     * Return an item rarity from EnumRarity
     */
    public EnumRarity getRarity(ItemStack par1ItemStack)
    {
        return EnumRarity.rare;
    }
	public EnumAction getItemUseAction(ItemStack par1ItemStack)
    {
        return EnumAction.drink;
    }
		public ItemStack onFoodEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.getFoodStats().addStats(this);
		--par1ItemStack.stackSize;
		par3EntityPlayer.addPotionEffect(new PotionEffect(Potion.invisibility.id, 5 * 20, 0));
		par3EntityPlayer.addPotionEffect(new PotionEffect(Potion.heal.id, 3 * 20, 0));
		par3EntityPlayer.addPotionEffect(new PotionEffect(Potion.confusion.id, 15 * 20, 0));

        if (par1ItemStack.stackSize <= 0)
        {
            return new ItemStack(mod_Drinks.Bottle);
        }
        else
        {
            par3EntityPlayer.inventory.addItemStackToInventory(new ItemStack(mod_Drinks.Bottle));
            return par1ItemStack;
        }
    }
	public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 32;
    }
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7)
    {
        return false;
    }
	
}
