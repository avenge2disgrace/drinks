package net.minecraft.src;
import java.util.Random;
import net.minecraft.src.forge.ITextureProvider;

public class BlockWoodPane extends Block implements ITextureProvider{
    public BlockWoodPane(int i, int j){
        super(i, j, Material.wood);
		blockIndexInTexture = j;
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 0.875F, 1.0F, 0.875F);
    }
    public int idDropped(int i, Random random, int j){
        return mod_Drinks.WoodPane.blockID;
    }
    public int quantityDropped(Random random){
        return 1;
    }
	public String getTextureFile(){
        return "/drinks/items.png";
    }
	public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return 1;
    }
	public boolean isOpaqueCube()
    {
        return false;
    }
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        float var5 = 0.125F;
        return AxisAlignedBB.getBoundingBoxFromPool((double)((float)par2 + var5), (double)par3, (double)((float)par4 + var5), (double)((float)(par2 + 1) - var5), (double)((float)(par3 + 1) - var5), (double)((float)(par4 + 1) - var5));
    }

    /**
     * Returns the bounding box of the wired rectangular prism to render.
     */
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        float var5 = 0.125F;
        return AxisAlignedBB.getBoundingBoxFromPool((double)((float)par2 + var5), (double)par3, (double)((float)par4 + var5), (double)((float)(par2 + 1) - var5), (double)(par3 + 1), (double)((float)(par4 + 1) - var5));
    }
}