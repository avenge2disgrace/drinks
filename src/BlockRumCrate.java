package net.minecraft.src;
import java.util.Random;
import net.minecraft.src.forge.ITextureProvider;

public class BlockRumCrate extends Block implements ITextureProvider{
    public BlockRumCrate(int i, int j){
        super(i, j, Material.wood);
    }
    public int idDropped(int i, Random random, int j){
        return mod_Drinks.RumCrate.blockID;
    }
	public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random){
		super.updateTick(par1World, par2, par3, par4, par5Random);
		if (par1World.getBlockLightValue(par2, par3, par4) < 4){
			//int i = par1World.getBlockMetadata(par2, par3, par4);
			
				if (par5Random.nextInt(3) > 0){
					Block var10 = mod_Drinks.SustainedRumCrate;
					par1World.setBlockWithNotify(par2, par3, par4, var10.blockID);
							//par1World.setBlockMetadataWithNotify(par2, par3, par4, i);
						
					
				}
			
		}
	}
    public int quantityDropped(Random random){
        return 1;
    }
	public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, int par5)
    {
        //this.checkBlockCoordValid(par1World, par2, par3, par4);
		//Block var10 = mod_Drinks.SustainedRumCrate;
		//par1World.setBlockWithNotify(par2, par3, par4, var10.blockID);
    }
	public String getTextureFile(){
        return "/drinks/items.png";
    }
	public int getBlockTextureFromSideAndMetadata(int i, int j){
        return getBlockTextureFromSide(i);
    }
	public int getBlockTextureFromSide(int i){
        if (i == 0){
            return 7;
        }
        if (i == 1){
            return 5;
        }
        else{
            return this.blockIndexInTexture;
        }
    }
}